from rest_framework import routers
from django.conf import settings
from django.contrib import admin
from django.urls import path, include
from apps.bills import views

# IMPORT APIs
from apps.builds import api

router = routers.DefaultRouter()
router.register('bills', views.BillViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('builds/', include('apps.builds.urls')),
    path('api/', include('simple_api.urls')),
    path('drf/', include(router.urls)),
]


if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path('__debug__/', include(debug_toolbar.urls))]
