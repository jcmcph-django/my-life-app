// Set the API URL
const api_url = "/api/";

// // My Favorite Filters
// Vue.filter('numeral', function(value, format){
//     return numeral(value).format(format);
// });
//
// Vue.filter('moment', function(value, format){
//     return moment(value).format(format);
// });
//
// Vue.filter('fromNow', function(value){
//     return moment(value).fromNow();
// });
//
// Vue.filter('toNow', function(value){
//     return moment(value).toNow();
// });


// Axios Mixin
const VueAxios = {
    created: function() {
        axios.defaults.xsrfCookieName = 'csrftoken';
        axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
    },
    methods: {

        async axios_get(params) {

            // An over complicated way to avoid "bracketing" of array parameters
            let search = new URLSearchParams();
            Object.keys(params).forEach(function(param){
                let value = params[param];
                if ( Object.prototype.toString.call(value) === '[object Array]' ) {
                    value.forEach(function(val){
                        search.append(param, val);
                    });
                } else {
                    search.append(param, value);
                }
            });

            return await axios.get(api_url, {params: search});

        },

        async axios_post(params) {
            return await axios({method: 'post', url: api_url, data: params});
        },

    },
}

// My Favorite Filters
const VueComputed = {
    computed: {
        moment() {
            return function(value, format) {
                return ( value !== null ) ? moment(value).format(format) : '';
            };
        },
        numeral() {
            return function(value, format) {
                return numeral(value).format(format);
            };
        },
        fromNow() {
            return function(value) {
                return moment(value).fromNow();
            };
        },
        toNow() {
            return function(value) {
                return moment(value).toNow();
            };
        },
    }
}
