const VuePaginateObject = function() {

    // Properties
    this.objects = []; // loaded, loading, updating?
    this.DisplayList = [];
    this.filters = []; // loaded, loading, updating?
    this.OrderBys = [];
    this.OrderBy = {key: '', asc: true};
    // todo: pagination

    // Computed Property
    Object.defineProperties(this, {

        FullList: {
            get: () => this.objects,
        },

        FilteredList: {
            get: () => this.objects.filter(o => o._isVisible)
        },

        IsFiltered: {
            get: function() {
                let self = this;
                return self.filters.filter(filter => filter.IsFiltered).length > 0;
            }
        },

        IsDirty: {
            get: function() {
                let self = this;
                return self.filters.filter(filter => filter.isDirty).length > 0;
            }
        }

    });

    // Computed Property (but takes a variable)
    this.LiveFilteredList = function(filter) {

        // Set Filter
        filter = filter || {key: '__'};

        // Alias
        let self = this;

        // Return a Filtered List based on current filters
        return self.objects.filter(function (o) {

            // initially set to visible
            let isVisible = true;

            // we only care about filters where "not all" are selected
            self.filters.filter(f => !f.allSelected).filter(f => f.key !== filter.key)
                .forEach(function (f) {
                    if (!isVisible) return false; // we've already determined that this row isn't visible, continue
                    let isVisibleInFilter = false;
                    f.definition(o).forEach(function (i) {
                        if (isVisibleInFilter) return true;  // we've already determined this row meets this filter's requirements
                        f.options.filter(s => s.isSelected).forEach(function (s) {
                            if (i.key === s.key) isVisibleInFilter = true;
                        });
                    });
                    isVisible = isVisibleInFilter
                });

            return isVisible;
        });

    }

    this.OrderingArrows = function(key) {
        let self = this;
        return ( key === self.OrderBy.key )
            ? ( self.OrderBy.asc )
                ? ['fas', 'fa-sort-up']
                : ['fas', 'fa-sort-down']
            : ['fas', 'fa-sort', 'text-muted'];
    }

    this.updateObjects = function(data) {

        let self = this;

        // Add Initial Values
        data.forEach(function(o){
            o._isVisible = true;
            o._OrderByValue = 0;
        });

        // Update the List
        self.objects = data;

        // Update the Filter List
        self.updateFilterOptions();

    }

    this.AddToObjects = function(data) {

        let self = this;

        // Add Initial Values
        data._isVisible = true;
        data._OrderByValue = 0;
        self.objects.push(data);

        // Update the Filter List
        self.updateFilterOptions();

    }

    this.addFilter = function(key, name, definition) {
        let self = this;
        self.filters.push(new VuePaginateFilter(key, name, definition, self));
    }

    this.ApplyFilters = function() {
        let self = this;
        self.filters.filter(filter => filter.isDirty).forEach(filter => filter.ApplyFilter(false));
        self.updateFilterOptions();
    }

    this.CancelApplyFilters = function() {
        let self = this;
        self.filters.filter(filter => filter.isDirty).forEach(filter => filter.CancelApplyFilter(false));
        self.updateFilterOptions();
    }

    this.ResetAllFilters = function() {
        let self = this;
        self.filters.forEach(filter => filter.ResetFilter(false));
        self.updateFilterOptions();
    };

    this.updateFilterOptions = function(DoUpdateFilterOptions) {

        // Check if DoUpdateFilterOptions is Passed -- Default is True
        if ( typeof(DoUpdateFilterOptions) === 'undefined' ) DoUpdateFilterOptions = true;

        // alias
        let self = this;

        // All Filters
        self.filters.forEach(function(filter) {

            // Get the keys that are currently in the options
            let existing_keys = [];
            filter.options.forEach(option => existing_keys.push(option.key));

            // Add New Options (We'll Keep Old Options)
            self.FullList.forEach(function(instance){
                filter.definition(instance).forEach(function(instance_option){
                    if ( existing_keys.indexOf(instance_option.key) < 0 ) {
                        filter.options.push(new VuePaginateFilterOption(instance_option.key, instance_option.name));
                        existing_keys.push(instance_option.key);
                    }
                });
            });

            // Hide All Options
            filter.options.filter(option => option.isVisible).forEach(option => option.isVisible = false);

            // Show Appropriate Options
            self.LiveFilteredList(filter).forEach(function(instance){
                filter.definition(instance).forEach(function(instance_option){
                    filter.options.filter(option => option.key === instance_option.key)
                        .forEach(option => option.isVisible = true);
                });
            });

        });

        // Update Object Is Visible Flags
        if (DoUpdateFilterOptions) self.updateObjectIsVisible();

    }

    this.updateObjectIsVisible = function() {
        let self = this;
        self.FullList.forEach(instance => instance._isVisible = false);
        self.LiveFilteredList().forEach(instance => instance._isVisible = true);
        self.UpdateDisplayList();
    }

    this.UpdateDisplayList = function() {
        let self = this;
        self.DisplayList = self.FilteredList.sort(function(a, b){
            if ( self.OrderBy.asc ) return ( a._OrderByValue <= b._OrderByValue ) ? -1 : 1;
            else return ( a._OrderByValue >= b._OrderByValue ) ? -1 : 1;
        });
    }

    // Add Order By
    this.AddOrderBy = function(key, name, definition, data_type, default_is_asc) {
        let self = this;
        self.OrderBys.push(new VuePaginateOrderBy(key, name, definition, data_type, default_is_asc));
    }

    this._GetOrderByFromKey = function(key) {
        let self = this;
        let ReturnOrderBy = false;
        self.OrderBys.filter(OrderBy => OrderBy.key === key).forEach(OrderBy => ReturnOrderBy = OrderBy);
        return ReturnOrderBy;
    }

    this.SetOrderBy = function(key) {
        let self = this;

        // Get the Order ByObject
        const OrderBy = self._GetOrderByFromKey(key);
        if ( !OrderBy ) return false;

        // Set the Values
        if ( self.OrderBy.key === key ) self.OrderBy.asc = !self.OrderBy.asc;
        else self.OrderBy = {key: OrderBy.key, asc: OrderBy.default_is_asc};
        self.objects.forEach(instance => instance._OrderByValue = OrderBy.definition(instance));

        // Update
        self.UpdateDisplayList();

    }

};

const VuePaginateFilter = function(key, name, definition, object) {

    // Properties
    this.key = key;
    this.name = name;
    this.definition = definition;
    this.object = object;
    this.options = [];

    // Computed Properties
    Object.defineProperties(this, {

        allSelected: {
            get: () => this.options.length === this.options.filter(o => o.isSelected).length,
            set: function() {
                let self = this;
                if ( self.allSelected ) self.options.forEach(o => o.isSelected = false);
                else self.options.filter(o => !o.allSelected).forEach(o => o.allSelected = true);
            }
        },

        allSelectedPre: {
            get: () => this.options.length === this.options.filter(o => o.isSelectedPre).length,
            set: function() {
                let self = this;
                if ( self.allSelectedPre ) self.options.forEach(o => o.isSelectedPre = false);
                else self.options.filter(o => !o.isSelectedPre).forEach(o => o.isSelectedPre = true);
            }
        },

        isDirty: {
            get: function() {  // TODO: Why did () notation fail??
                let self = this;
                return self.options.filter(o => o.isSelected !== o.isSelectedPre).length > 0;
            }
        },

        IsFiltered: {
            get: function() {
                let self = this;
                return self.options.filter(option => (option.isVisible && !option.isSelected)).length > 0;
            }
        },

        VisibleOptionsLength: {
            get: function() {
                let self = this;
                return self.options.filter(option => option.isVisible).length;
            }
        },

    });

    // Returns Filter selections
    this.CancelApplyFilter = function() {

        // Alias
        let self = this;

        // Cancel Changes
        self.options.filter(o => o.isSelectedPre !== o.isSelected).forEach(o => o.isSelectedPre = o.isSelected);

    };

    // Applies Filters based on current selection
    this.ApplyFilter = function(DoUpdateFilterOptions) {

        // Check if DoUpdateFilterOptions is Passed -- Default is True
        if ( typeof(DoUpdateFilterOptions) === 'undefined' ) DoUpdateFilterOptions = true;

        // Alias
        let self = this;

        // Apply Filters
        self.options.filter(o => o.isSelectedPre !== o.isSelected).forEach(o => o.isSelected = o.isSelectedPre);

        // Update Filter Options (Unless Canceled by Caller)
        if ( DoUpdateFilterOptions ) self.object.updateFilterOptions();
    };

    // Resets the Filter
    this.ResetFilter = function(DoUpdateFilterOptions) {

        // Check if DoUpdateFilterOptions is Passed -- Default is True
        if ( typeof(DoUpdateFilterOptions) === 'undefined' ) DoUpdateFilterOptions = true;

        // Alias
        let self = this;

        // Cancel Any Live Selections
        self.CancelApplyFilter();

        // Reset the Filter
        self.options.filter(option => !option.isSelected).forEach(function(option){
            option.isSelected = true;
            option.isSelectedPre = true;
        });

        // Update Filter Options (Unless Canceled by Caller)
        if ( DoUpdateFilterOptions ) self.object.updateFilterOptions();

    };
};

const VuePaginateFilterOption = function(key, name) {
    this.key = key;
    this.name = name;
    this.isSelected = true;
    this.isSelectedPre = true;
    this.isVisible = true;
};

const VuePaginateOrderBy = function(key, name, definition, data_type, default_is_asc) {
    this.key = key;
    this.name = name;
    this.definition = definition;
    this.data_type = data_type;
    this.default_is_asc = default_is_asc;
};
