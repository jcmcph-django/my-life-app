# Generated by Django 3.1.6 on 2021-02-12 06:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builds', '0003_auto_20210211_2348'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='actual_complete_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='actual_start_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='expected_complete_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='project',
            name='expected_start_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
