# Generated by Django 3.1.6 on 2021-02-12 05:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builds', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='project',
            options={'ordering': ('default_order', 'name')},
        ),
        migrations.AlterModelOptions(
            name='projectstatus',
            options={'ordering': ('default_order', 'name'), 'verbose_name_plural': 'Project statuses'},
        ),
        migrations.AlterModelOptions(
            name='supply',
            options={'verbose_name_plural': 'Supplies'},
        ),
        migrations.AlterModelOptions(
            name='supplycategory',
            options={'ordering': ('default_order', 'name'), 'verbose_name_plural': 'Supply categories'},
        ),
        migrations.AddField(
            model_name='project',
            name='total_extended_price',
            field=models.DecimalField(decimal_places=3, default=0, max_digits=20),
        ),
        migrations.AddField(
            model_name='supply',
            name='default_order',
            field=models.DecimalField(decimal_places=5, default=0, max_digits=10),
        ),
    ]
