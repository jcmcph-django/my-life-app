from django.apps import AppConfig


class BuildsConfig(AppConfig):
    name = 'apps.builds'
