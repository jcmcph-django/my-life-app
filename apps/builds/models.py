import uuid
from django.db import models
from simple_api.abstract import SerializableModel
from autoslug import AutoSlugField


class AbsUUID(SerializableModel):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)

    class Meta:
        abstract = True


class AbsCreateUpdateTime(SerializableModel):
    create_time = models.DateTimeField(auto_now_add=True)
    update_time = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class AbsNameAndSlug(SerializableModel):
    name = models.CharField(max_length=50, unique=True)
    slug = AutoSlugField(populate_from='name', unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class AbsChoice(AbsUUID, AbsNameAndSlug):

    default_order = models.IntegerField(default=0)

    @property
    def abstract_serializable_fields(self):
        return ['default_order', 'name', 'slug', 'uuid']

    @property
    def serializable_fields(self):
        return self.abstract_serializable_fields

    class Meta:
        abstract = True


class ProjectRoom(AbsChoice):
    class Meta:
        ordering = ('default_order', 'name')


class ProjectStatus(AbsChoice):
    class Meta:
        ordering = ('default_order', 'name')
        verbose_name_plural = 'Project statuses'


class SupplyCategory(AbsChoice):
    class Meta:
        ordering = ('default_order', 'name')
        verbose_name_plural = 'Supply categories'


class SupplyStore(AbsChoice):
    class Meta:
        ordering = ('default_order', 'name')


class Project(AbsUUID, AbsCreateUpdateTime, AbsNameAndSlug):
    room = models.ForeignKey(ProjectRoom, on_delete=models.PROTECT)
    status = models.ForeignKey(ProjectStatus, on_delete=models.PROTECT)
    total_supply_count = models.IntegerField(default=0)
    total_supply_quantity = models.DecimalField(max_digits=20, decimal_places=3, default=0)
    total_extended_price = models.DecimalField(max_digits=20, decimal_places=3, default=0)

    expected_start_date = models.DateField(blank=True, null=True)
    actual_start_date = models.DateField(blank=True, null=True)
    expected_complete_date = models.DateField(blank=True, null=True)
    actual_complete_date = models.DateField(blank=True, null=True)

    default_order = models.DecimalField(max_digits=10, decimal_places=5, default=0)

    @property
    def project_date(self):
        return (
            self.actual_complete_date or
            self.actual_start_date or
            self.expected_start_date
        )

    @property
    def serializable_fields(self):
        return ['uuid', 'create_time', 'update_time', 'name', 'slug', 'room', 'status',
                'total_supply_count', 'total_supply_quantity', 'total_extended_price',
                'expected_start_date', 'actual_start_date', 'expected_complete_date', 'actual_complete_date',
                'project_date', 'default_order']

    class Meta:
        ordering = ('default_order', 'name')


class ProjectFile(AbsUUID, AbsCreateUpdateTime, AbsNameAndSlug):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)  # I want the S3 file to be deleted first
    file = models.FileField(upload_to='builds/project-file/')


class Supply(AbsUUID, AbsCreateUpdateTime):

    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    name = models.CharField(max_length=150)
    description = models.TextField(blank=True)

    category = models.ForeignKey(SupplyCategory, on_delete=models.PROTECT)

    quantity = models.DecimalField(max_digits=20, decimal_places=3, default=0)
    unit_price = models.DecimalField(max_digits=20, decimal_places=3, default=0)

    image_width = models.IntegerField(blank=True, null=True)
    image_height = models.IntegerField(blank=True, null=True)
    image = models.ImageField(blank=True, null=True, width_field='image_width', height_field='image_height',
                              upload_to='builds/supply-image/')

    @property
    def extended_price(self):
        return self.quantity * self.unit_price

    store = models.ForeignKey(SupplyStore, on_delete=models.SET_NULL, blank=True, null=True)
    location = models.CharField(max_length=50, blank=True)
    link = models.URLField(blank=True, null=True)

    is_marked_off = models.BooleanField(default=False)
    default_order = models.DecimalField(max_digits=10, decimal_places=5, default=0)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):

        # Run The Default Save Action
        super().save(*args, **kwargs)

        # Calculate Extended Price
        total_extended_price = Supply.objects.filter(
            project=self.project
        ).aggregate(
            total_supply_count=models.Count('id'),
            total_supply_quantity=models.Sum('quantity'),
            total_extended_price=models.Sum(models.F('quantity')*models.F('unit_price')),
        )

        # Update
        project = Project.objects.get(pk=self.project_id)
        project.total_supply_count = total_extended_price.get('total_supply_count', 0)
        project.total_supply_quantity = total_extended_price.get('total_supply_quantity', 0)
        project.total_extended_price = total_extended_price.get('total_extended_price', 0)
        project.save()

    @property
    def serializable_fields(self):
        return ['uuid', 'create_time', 'update_time', 'name', 'description',
                'category', 'quantity', 'unit_price', 'extended_price', 'store', 'location', 'link',
                'is_marked_off', 'default_order']

    class Meta:
        verbose_name_plural = 'Supplies'
