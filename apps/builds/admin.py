from django.contrib import admin
from .models import Project, ProjectRoom, ProjectStatus, Supply, SupplyCategory, SupplyStore, ProjectFile


admin.site.register(Project)
admin.site.register(ProjectRoom)
admin.site.register(ProjectStatus)
admin.site.register(Supply)
admin.site.register(SupplyCategory)
admin.site.register(SupplyStore)
admin.site.register(ProjectFile)
