from simple_api import api
from ..models import Project, Supply, ProjectRoom, ProjectStatus, SupplyCategory, SupplyStore


def get_by_uuid(model, uuid):
    try:
        return model.objects.get(uuid=uuid)
    except model.DoesNotExist:
        return None


@api.register()
def api_get_builds_choices(request):
    return api.api({
        'project_rooms': ProjectRoom.serializable.all().serialize(),
        'project_statuses': ProjectStatus.serializable.all().serialize(),
        'supply_categories': SupplyCategory.serializable.all().serialize(),
        'supply_stores': SupplyStore.serializable.all().serialize(),
    })


@api.register()
def api_get_builds_project_list(request):

    def add_supplies(project):
        supplies = {'supplies': [], 'updating': False}
        return {**project, **supplies}

    projects = Project.serializable.select_related('room', 'status').all()
    serialized = [add_supplies(project) for project in projects.serialize()]
    return api.api(serialized)


def get_project(project_uuid):
    # TODO: Handle Does Not Exist
    project = Project.serializable.select_related('room', 'status').get(uuid=project_uuid)
    supplies = Supply.serializable.select_related('store', 'category').filter(project=project)
    serialized = {**project.serialize(), **{'supplies': supplies.serialize(), 'updating': False}}
    return serialized


@api.register()
def api_get_builds_project(request):
    project = get_project(request.GET.get('project_uuid'))
    return api.api(project)


@api.register()
def api_get_builds_supplies(request):

    # Queryset
    supplies = Supply.serializable.select_related(
        'store', 'category', 'project__room', 'project__status'
    ).filter(
        project__actual_complete_date__isnull=True
    ).order_by(
        'project__expected_start_date',
        'store',
        'name',
    )

    # Fields
    fields = [
        'uuid', 'create_time', 'update_time', 'name', 'description',
        'category', 'quantity', 'unit_price', 'extended_price', 'store', 'location', 'link',
        'is_marked_off', 'default_order', 'project'
    ]

    # Return
    return api.api(supplies.serialize(fields=fields))


@api.register()
def api_post_builds_add_project(request):

    # Parse Body
    body = api.parse_body(request)
    new = body.get('project', {})

    # Create The Project
    project = Project.objects.create(
        name=new.get('name'),
        room=get_by_uuid(ProjectRoom, new.get('room', {}).get('uuid')),
        status=get_by_uuid(ProjectStatus, new.get('status', {}).get('uuid')),
        expected_start_date=new.get('expected_start_date'),
        actual_start_date=new.get('actual_start_date'),
        expected_complete_date=new.get('expected_complete_date'),
        actual_complete_date=new.get('actual_complete_date'),
    )
    project.save()

    # Return
    return api.api(get_project(project.uuid))


@api.register()
def api_post_builds_update_project(request):

    # Parse Body
    body = api.parse_body(request)
    updated = body.get('project', {})

    # Get The Project # TODO: Handle DoesNotExist
    project = Project.objects.get(uuid=updated.get('uuid'))

    # Update The Values
    project.name = updated.get('name')
    project.room = ProjectRoom.objects.get(uuid=updated.get('room', {}).get('uuid'))
    project.status = ProjectStatus.objects.get(uuid=updated.get('status', {}).get('uuid'))
    project.expected_start_date = updated.get('expected_start_date')
    project.actual_start_date = updated.get('actual_start_date')
    project.expected_complete_date = updated.get('expected_complete_date')
    project.actual_complete_date = updated.get('actual_complete_date')
    project.save()

    # Return
    return api.api(get_project(project.uuid))


@api.register()
def api_post_builds_update_supply(request):

    # Parse Body
    body = api.parse_body(request)
    updated = body.get('supply', {})

    # Get the Supply
    supply = Supply.objects.get(uuid=updated.get('uuid'))

    # Update The Values
    supply.name = updated.get('name', supply.name)
    supply.description = updated.get('description', supply.description)
    supply.quantity = updated.get('quantity', supply.quantity)
    supply.unit_price = updated.get('unit_price', supply.unit_price)
    supply.location = updated.get('location', supply.location)
    supply.link = updated.get('link', supply.link)
    supply.is_marked_off = updated.get('is_marked_off', supply.is_marked_off)
    supply.category = get_by_uuid(SupplyCategory, updated.get('category', {}).get('uuid')) or supply.category
    supply.store = get_by_uuid(SupplyStore, updated.get('store', {}).get('uuid')) or supply.store
    supply.save()

    # Get the Project UUID
    project_uuid = Project.objects.get(pk=supply.project.id).uuid

    # Return
    return api.api({
        'project': get_project(project_uuid),
        'supply': Supply.serializable.select_related('category', 'store').get(pk=supply.id).serialize()
    })


@api.register()
def api_post_builds_add_supply(request):

    # Parse Body
    body = api.parse_body(request)
    project = body.get('project', {})
    new = body.get('supply', {})

    # Get The Project  # TODO: DoesNotExist
    project = get_by_uuid(Project, project.get('uuid'))

    # Create the Supply
    supply = Supply.objects.create(
        project=project,
        name=new.get('name'),
        description=new.get('description', ''),
        quantity=new.get('quantity', 0),
        unit_price=new.get('unit_price', 0),
        location=new.get('location', ''),
        link=new.get('link', ''),
        category=get_by_uuid(SupplyCategory, new.get('category', {}).get('uuid')),
        store=get_by_uuid(SupplyStore, new.get('store', {}).get('uuid')),
    )
    supply.save()

    # Return
    return api.api({
        'project': get_project(project.uuid),
        'supply': Supply.serializable.select_related('category', 'store').get(pk=supply.id).serialize()
    })


@api.register()
def api_post_builds_add_choice(request):

    body = api.parse_body(request)

    types = {
        'supply_store': SupplyStore,
        'supply_category': SupplyCategory,
    }

    types[body.get('choice', {}).get('type')].objects.create(
        name=body.get('choice', {}).get('name', '')
    )

    return api.api()
