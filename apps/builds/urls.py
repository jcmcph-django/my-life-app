from django.urls import path
from django.views.generic import TemplateView


urlpatterns = [
    path('supplies/', TemplateView.as_view(template_name="builds/supplies/supplies.html")),
    path('', TemplateView.as_view(template_name="builds/home/index.html")),
]
