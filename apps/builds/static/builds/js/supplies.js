let doubleClickTimeout;
const app = Vue.createApp({
    mixins: [VueAxios, VueComputed],
    data() {
        return {
            supplies: new VuePaginateObject,
            supply: {
                store: {},
                category: {},
                link: '',
            },
            choices: {
                'project_rooms': [],
                'project_statuses': [],
                'supply_categories': [],
                'supply_stores': [],
            },
            check_double_click: '',
        }
    },
    mounted() {

        let self = this;

        self.getChoices();

        // Add Filters
        self.supplies.addFilter('marked', 'Purchased', instance => ([{key: instance.is_marked_off, name: (instance.is_marked_off)?'Yes':'No'}]));
        self.supplies.addFilter('store', 'Store', instance => ([{key: self.storeKey(instance), name: self.storeName(instance)}]));
        self.supplies.addFilter('category', 'Category', instance => ([{key: instance.category.uuid, name: instance.category.name}]));
        self.supplies.addFilter('project', 'Project', instance => ([{key: instance.project.uuid, name: instance.project.name}]));

        // Get Supplies
        self.getSupplies();

    },
    methods: {

        updateObjectWithKeys(objectToUpdate, newValues) {
            for ( const [key, value] of Object.entries(newValues) )
                objectToUpdate[key] = value;
        },

        // Get Choices
        getChoices() {
            let self = this;
            self.axios_get({action_name: 'builds-choices'})
                .then(response => self.choices = response.data.data);

        },

        // Get Supplies
        getSupplies() {
            let self = this;
            self.axios_get({action_name: 'builds-supplies'})
                .then(response => self.supplies.updateObjects(response.data.data));
        },

        // Supply Clicked Listener
        supply_clicked(supply) {

            // this alias
            let self = this;

            // clear timeout
            clearTimeout(doubleClickTimeout);

            // check if double click
            if ( self.check_double_click === supply.uuid ) {
                doubleClickTimeout = setTimeout(self.toggleMarkedOff, 0, supply);
            } else {
                doubleClickTimeout = setTimeout(self.showSupplyDetails, 250, supply);
            }

            // Set the Check Double Click Variable
            self.check_double_click = supply.uuid;

        },

        // Show Supply Details
        showSupplyDetails(supply) {
            this.check_double_click = '';
            this.supply = supply;
            let supplyModal = new bootstrap.Modal(document.getElementById('supply-modal'));
            supplyModal.show();
        },

        // Toggle Marked Off
        toggleMarkedOff(supply) {
            this.check_double_click = '';
            supply.is_marked_off = !supply.is_marked_off;
            this.updateSupply(supply);
        },

        // Update Supply
        updateSupply(supply) {
            let self = this;
            self.axios_post({action_name: 'builds-update-supply', supply: supply})
                .then(function(response){
                    self.updateObjectWithKeys(supply, response.data.data.supply);
                });
        },

    },
    computed: {
        storeKey() {
            return function(supply) {
                return ( supply.hasOwnProperty('store') ) ? supply.store.uuid : '';
            };
        },
        storeName() {
            return function(supply) {
                return ( supply.hasOwnProperty('store') ) ? supply.store.name : '';
            };
        }
    }
});

const vm = app.mount('#app');
