const app = Vue.createApp({
    mixins: [VueAxios, VueComputed],
    data() {
        return {
            projects: new VuePaginateObject,
            project: {
                room: {},
                status: {},
                supplies: [],
            },
            supply: {
                store: {},
                category: {},
                link: '',
            },
            choices: {
                'project_rooms': [],
                'project_statuses': [],
                'supply_categories': [],
                'supply_stores': [],
            },
            addChoice: {
                name: '',
                type: '',
            }
        }
    },
    mounted() {

        // Alias
        let self = this;

        // Set the List Filters
        self.projects.addFilter('room','Room', i=>([{key: i.room.uuid, name: i.room.name}]));
        self.projects.addFilter('status','Status', i=>([{key: i.status.uuid, name: i.status.name}]));
        self.projects.addFilter('supplies', 'Has Supplies', i=>[{key: i.total_supply_count>0, name:(i.total_supply_count>0)?'Yes':'No'}]);

        // Set the Order By Functions
        self.projects.AddOrderBy('name', 'Name', i=>i.name, String, true);
        self.projects.AddOrderBy('room', 'Room', i=>i.room.name, String, true);
        self.projects.AddOrderBy('supplies', 'Supplies', i=>i.total_supply_count, Number, false);
        self.projects.AddOrderBy('cost', 'Projected Cost', i=>i.total_extended_price, Number, false);
        self.projects.AddOrderBy('status', 'Status', i=>i.status.name, String, true);
        self.projects.AddOrderBy('project_date', 'Date', i=>i.project_date, Date, true);

        // Starting Models
        self.getChoices();
        self.getProjects();

    },
    methods: {

        updateObjectWithKeys(objectToUpdate, newValues) {
            for ( const [key, value] of Object.entries(newValues) )
                objectToUpdate[key] = value;
        },

        // Get Choices
        getChoices() {
            let self = this;
            self.axios_get({action_name: 'builds-choices'})
                .then(response => self.choices = response.data.data);

        },

        // Get Projects
        getProjects() {

            // alias to this
            let self = this;

            // Get Data
            self.axios_get({action_name: 'builds-project-list'})
                .then(r => self.projects.updateObjects(r.data.data));

        },

        // Get Project Data
        getProject(project, event) {
            event.preventDefault();
            let self = this;
            project.updating = true;
            self.project = {};
            self.project = project;
            self.axios_get({action_name: 'builds-project', project_uuid: project.uuid})
                .then(response => self.updateObjectWithKeys(project, response.data.data));

        },

        // Start Add New Project
        startAddNewProject() {
            this.project = {room: {}, status: {}, supplies: []};
        },

        AddNewProject(project) {
            let self = this;
            self.axios_post({action_name: 'builds-add-project', project: project})
                .then(function(response){
                    self.projects.AddToObjects(response.data.data);
                });
        },

        // Update Project
        updateProject(project) {
            let self = this;
            self.axios_post({action_name: 'builds-update-project', project: project})
                .then(response => self.updateObjectWithKeys(project, response.data.data));
        },

        // Show Supply
        showSupply(supply, event) {
            event.preventDefault();
            this.supply = supply;
        },

        // Start Add New Supply
        StartAddNewSupply() {
            this.supply =  {
                store: {},
                category: {},
                link: '',
            };
        },

        // Add New Supply
        AddNewSupply(project, supply) {
            let self = this;
            self.axios_post({action_name: 'builds-add-supply', project: project, supply: supply})
                .then(function(response){
                    self.projects.objects.filter(p => p.uuid === response.data.data.project.uuid).forEach(function(p){
                        self.updateObjectWithKeys(p, response.data.data.project);
                    });

                });
        },

        // Update Supply
        updateSupply(supply) {
            let self = this;
            self.axios_post({action_name: 'builds-update-supply', supply: supply})
                .then(function(response){
                    self.projects.objects.filter(p => p.uuid === response.data.data.project.uuid).forEach(p => self.updateObjectWithKeys(p, response.data.data.project));
                    self.updateObjectWithKeys(supply, response.data.data.supply);
                });
        },

        // Start Add Choice
        StartAddChoice(type) {
            this.addChoice.name = '';
            this.addChoice.type = type;
        },

        // Add Choice
        AddChoice() {
            let self = this;
            self.axios_post({action_name: 'builds-add-choice', choice: self.addChoice})
                .then(function(response){
                    self.getChoices();
                });
        }

    },
    computed: {
        choiceName() {
            return function(obj) {
                if ( obj ) return obj.name;
                return '';
            };
        }
    }

})

const vm = app.mount('#app');
