from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.management.base import BaseCommand

# Get User Model
User = get_user_model()


class Command(BaseCommand):

    def handle(self, *args, **options):

        # Confirm DEBUG = True
        if not settings.DEBUG:
            self.stderr.write(self.style.ERROR('Admin User can only be created when DEBUG=True'))
            return False

        # Admin User Does not Exist
        if not User.objects.filter(username='admin').exists():

            # Create User and Alert
            User.objects.create_superuser('admin', 'admin@admin.com', 'admin')
            self.stdout.write(self.style.SUCCESS('Admin user has been created'))

        else:

            self.stdout.write(self.style.WARNING('Admin user already exists'))

