from django.apps import AppConfig


class MylifeConfig(AppConfig):
    name = 'apps.mylife'
    verbose_name = 'My Life'
