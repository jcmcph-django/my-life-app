from django.db import models


class ChoiceActiveManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


# Choice App
class Choice(models.Model):
    is_active = models.BooleanField(default=True)
    choice_text = models.CharField('Text', max_length=255)
    choice_group = models.CharField('Group', max_length=30, db_index=True)
    choice_key = models.CharField('Key', max_length=30, db_index=True)
    choice_sort_order = models.IntegerField('Order', default=0)
    id = models.CharField(max_length=61, primary_key=True, blank=True)
    choice_parent = models.ForeignKey('self', name='Parent', on_delete=models.PROTECT, blank=True, null=True)

    objects = models.Manager()
    active = ChoiceActiveManager()

    @property
    def gen_id(self):
        return self.id or f'{self.choice_group}.{self.choice_key}'

    def __str__(self):
        return f'{self.choice_text}'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.id = self.gen_id

    def save(self, *args, **kwargs):
        self.id = self.gen_id
        super().save(*args, **kwargs)

    class Meta:

        ordering = (
            'choice_group',
            'choice_sort_order',
            'choice_text',
        )

        unique_together = (
            ('choice_group', 'choice_key'),
            ('choice_group', 'choice_text'),
        )
