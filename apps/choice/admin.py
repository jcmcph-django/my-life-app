from django.contrib import admin
from apps.choice.models import Choice


# Register your models here.
@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    search_fields = ('choice_group', 'choice_key', 'choice_text')

    list_filter = ('choice_group', 'is_active')
    list_display = ('id', 'choice_group', 'choice_sort_order', 'choice_key', 'choice_text')

    save_on_top =  True
