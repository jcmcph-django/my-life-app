from dateutil.relativedelta import relativedelta
from django.db import models
from django.utils import timezone
from jcm_django_abstract import (
    AbstractUniqueNameSlug, AbstractUUID, AbstractDeleteUser, AbstractForeignKeySelect, AbstractStringPK
)


class Payee(AbstractForeignKeySelect):
    class Meta:
        ordering = ('default_order', 'name')


class BankAccount(AbstractForeignKeySelect):
    class Meta:
        ordering = ('name', )


class BillCategory(AbstractForeignKeySelect):
    class Meta:
        ordering = ('name', )
        verbose_name_plural = 'Bill categories'


class PaymentStatus(AbstractForeignKeySelect, AbstractStringPK):
    class Meta:
        ordering = ('default_order', )
        verbose_name_plural = 'Payment statuses'


class Frequency(AbstractForeignKeySelect, AbstractStringPK):
    class Meta:
        ordering = ('default_order', )
        verbose_name_plural = 'Frequencies'


class Bill(AbstractUniqueNameSlug, AbstractUUID, AbstractDeleteUser):

    # Properties
    payee = models.ForeignKey(Payee, on_delete=models.PROTECT, blank=True)
    bill_category = models.ForeignKey(BillCategory, blank=True, null=True, on_delete=models.PROTECT)

    # Interval & Frequency
    start_date = models.DateField(blank=True, null=True)
    interval = models.IntegerField(default=1)
    frequency = models.ForeignKey(Frequency, on_delete=models.PROTECT)

    # Defaults
    expected_amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    default_bank_account = models.ForeignKey(BankAccount, on_delete=models.PROTECT, blank=True, null=True)

    # Flags
    is_active = models.BooleanField(default=True)
    is_auto_pay = models.BooleanField(default=False)

    # Links
    history_link = models.URLField(blank=True, null=True)
    pay_online_link = models.URLField(blank=True, null=True)

    # Related Models
    previous_paid_invoice = models.ForeignKey('Invoice', blank=True, null=True, on_delete=models.SET_NULL,
                                              related_name='previous_paid_invoice', editable=False)
    current_invoice = models.ForeignKey('Invoice', blank=True, null=True, on_delete=models.SET_NULL,
                                        related_name='currently_paid_invoice', editable=False)

    def save(self, *args, **kwargs):

        # Create Variable For if created
        created = self._state.adding

        # Get Previous Paid & Current Invoice
        self.previous_paid_invoice, self.current_invoice = get_previous_current_invoice(self)

        # If Is Active and No Current, Create It
        # Unless it's a new record -- need to save first
        if self.is_active and not self.current_invoice and not created:
            self.current_invoice = create_current_invoice(self)

        # Save It
        super().save(*args, **kwargs)

        # Create Current Invoice
        if created:
            create_current_invoice(self)


class Invoice(AbstractUUID, AbstractDeleteUser):
    bill = models.ForeignKey(Bill, on_delete=models.CASCADE)
    due_date = models.DateField()
    due_amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    paid_date = models.DateField(blank=True, null=True)
    paid_amount = models.DecimalField(max_digits=8, decimal_places=2, default=0)
    bank_account = models.ForeignKey(BankAccount, blank=True, null=True, on_delete=models.PROTECT)
    payment_status = models.ForeignKey(PaymentStatus, on_delete=models.PROTECT, db_index=True)
    notes = models.TextField(blank=True)
    paperwork = models.FileField(blank=True, null=True, upload_to='bills/invoice/paperwork/')


def add_date(start_date, interval=None, frequency=1):

    if interval == 'W':
        return start_date + relativedelta(weeks=frequency)

    elif interval == 'M':
        return start_date + relativedelta(months=frequency)

    elif interval == 'Y':
        return start_date + relativedelta(years=frequency)

    else:
        return start_date + relativedelta(days=frequency)


def calculate_current_due_date(bill):

    # If Not Active, Then There is No Due Date
    if not bill.is_active:
        return None

    # There is already a due date -- just use that... obviously
    elif bill.current_invoice and bill.current_invoice.due_date:
        return bill.current_invoice.due_date

    # Compare Versus the Last Due Date
    elif bill.previous_paid_invoice and bill.previous_paid_invoice.due_date:
        return add_date(bill.previous_paid_invoice.due_date, bill.frequency_id, bill.interval)

    # Is there a start date?
    elif bill.start_date:
        return bill.start_date

    # Just return today
    else:
        return timezone.now().date()


def create_current_invoice(bill):

    # If there is already a current invoice, just return that
    # This also works for inactive -- it will just likely return none
    if bill.current_invoice is not None or not bill.is_active:
        return bill.current_invoice

    # Get the "Future" payment status
    payment_status_future, payment_status_future_created = PaymentStatus.objects.get_or_create(
        id='F', defaults={'name': 'Future', 'default_order': 1}
    )

    # Get the Due Date and Paid Date
    due_date = calculate_current_due_date(bill)
    paid_date = due_date if bill.is_auto_pay else None

    # Create The Record
    current_invoice = Invoice.objects.create(
        bill=bill,
        due_date=due_date,
        due_amount=bill.expected_amount,
        paid_date=paid_date,
        paid_amount=bill.expected_amount,
        bank_account=bill.default_bank_account,
        payment_status=payment_status_future,
    )

    # Save
    bill.current_invoice = current_invoice
    bill.save()

    # Return
    return current_invoice


def get_previous_current_invoice(bill):

    previous_qs = Invoice.objects.filter(bill=bill, payment_status_id='P').order_by('-due_date')[:1]
    current_qs = Invoice.objects.filter(bill=bill).exclude(payment_status_id='P').order_by('due_date')[:1]

    return (
        previous_qs[0] if previous_qs.count() > 0 else None,
        current_qs[0] if current_qs.count() > 0 else None
    )
