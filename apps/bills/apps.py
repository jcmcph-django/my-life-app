from django.apps import AppConfig


class BillsConfig(AppConfig):
    name = 'apps.bills'
    verbose_name = 'Bills'
