from .models import Bill
from rest_framework import serializers


class BillSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Bill
        fields = ['uuid', 'name']
