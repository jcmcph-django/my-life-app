from django.contrib import admin
from .models import Bill, Invoice, Payee, BillCategory, BankAccount


# Register your models here.
admin.site.register(Bill)
admin.site.register(Invoice)
admin.site.register(Payee)
admin.site.register(BillCategory)
admin.site.register(BankAccount)
