# My Life App
This is an app I'm creating to help manage aspects of my life that I currently manage in spreadsheets, notebooks,
sheets of scratch paper, and just in my head. It's based on my Django Boilerplate with some modifications.
I fully expect this to grow. It's a project not only to stay more organized,
but also learn different aspects of Django, various JS frameworks, and deploying in different cloud environments.

Do not be surprised if different areas look different. That's sort of the point, in a way.
Not that I relish in inconsistencies, but I expect each little app within this project will be better than the one before.
I mean... that's sort of the point of this.


### Current Django Version
This is currently running Django 3.1.6, but it was originally created using 2.2.13, then the package was updated.
This means that the startproject is based on an older version of Django, currently.
The only big different I am currently aware of is the lack of ASGI file.
I will ultimately rerun the startproject and see what breaks --
probably about the time that 3.2 LTS is released.


### Builds
Builds is the first mini-app I'm building.
I'm trying to improve on a process to organize my woodworking hobby projects better than I am doing using multiple Google Sheets.
Not that using Sheets couldn't work, but right now, I have a master list of projects I plan to build
each linking to a new sheet listing the lumber, hardware, and other supplies needed to successfully complete that project.
It's disjointed, and not only is technically limiting, but it's woodworking inefficient too as it's difficult to see
potential similarities in supplies required, and I end up buying these in smaller quantities that prevent price breaks.
With the cost of lumber skyrocketing early in 2021, any way to save a buck on a hobby that I haven't turned into a
revenue-generating side hustle, yet, it's best to be smart about these things. Heck, even if I was generating revenue,
I should smart about it.
